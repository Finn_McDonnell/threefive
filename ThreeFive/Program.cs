﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ThreeFive
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i <= 30; i++)
            {
                System.Console.Write(i.ToString()); //Always print the number
                if (multipleOf3(i)) System.Console.Write(" three"); //Print the string ' Three' conditionally (with space for readability).
                if (multipleOf5(i)) System.Console.Write(" five"); //Print the string ' Five' conditionally (with space for readability).
                System.Console.Write("\n"); //New line for next iteration.
            }

        }

        static bool multipleOf3(int number) // Determine if the given number is a multiple of 3
        {
            if (number % 3 == 0) return true;
            else return false;
        }

        static bool multipleOf5(int number) // Determine if the given number is a multiple of 5
        {
            if (number % 5 == 0) return true;
            else return false;
        }
    }
}
